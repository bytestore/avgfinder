package avgfinder

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"unicode"
)

// ReadCSVFile reads the given CSV file and adds its IDs and values to the given map.
func ReadCSVFile(fileName string, valueMap map[string]float64) {
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	lineno := 0
	for scanner.Scan() {
		line := scanner.Text()
		lineno++
		id, value, err := parseLine(line)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error on line %s:%d : %s\n", filepath.Base(fileName), lineno, err)
			os.Exit(1)
		}

		err = addToMap(valueMap, id, value)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error on line %s:%d : %s\n",
				filepath.Base(fileName), lineno, err)
			os.Exit(1)
		}
	}

	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func parseLine(line string) (string, float64, error) {
	parts := strings.Split(line, ",")
	if len(parts) != 2 {
		return "", 0, fmt.Errorf("Wrong format: %s", line)
	}

	id := parts[0]
	if checkID(id) != nil {
		return "", 0, fmt.Errorf("Invalid ID: %s", id)
	}

	value, err := strconv.ParseFloat(parts[1], 64)
	if err != nil {
		return "", 0, fmt.Errorf("Invalid value: %s", parts[1])
	}

	if value <= 0 {
		return "", 0, fmt.Errorf("Value must be positive: %f", value)
	}

	return id, value, nil
}

func checkID(id string) error {
	for i, r := range id {
		if i == 0 && !unicode.IsLetter(r) {
			return fmt.Errorf("ID should start with at letter: %s", id)
		} else if !(unicode.IsDigit(r) || unicode.IsLetter(r)) {
			return fmt.Errorf("ID should only contain letters and digits: %s", id)
		}
	}
	return nil
}

func addToMap(m map[string]float64, id string, value float64) error {
	if _, ok := m[id]; ok {
		err := fmt.Errorf("Duplicate ID: %s", id)
		return err
	}
	m[id] = value
	return nil
}

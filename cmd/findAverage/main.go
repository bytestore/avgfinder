package main

import (
	"fmt"
	"os"
	"path/filepath"

	"avgfinder"
)

func main() {

	if len(os.Args) != 3 {
		fmt.Fprintf(os.Stderr, "Usage: %s <weights.csv> <prices.csv>\n", filepath.Base(os.Args[0]))
		os.Exit(1)
	}

	weightMap := make(map[string]float64)
	avgfinder.ReadCSVFile(os.Args[1], weightMap)

	priceMap := make(map[string]float64)
	avgfinder.ReadCSVFile(os.Args[2], priceMap)

	ids := avgfinder.FindAverageItems(weightMap, priceMap)
	for _, id := range ids {
		fmt.Println(id)
	}
}

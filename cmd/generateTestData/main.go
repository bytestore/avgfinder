package main

import (
	"flag"
	"fmt"
	"math/rand"
	"time"
)

// Generate produces n lines of test data for the CSV files.
func generate(n int, maxValue int) []string {

	result := []string{}

	rand.Seed(time.Now().UTC().UnixNano())

	for i := 1; i <= n; i++ {
		value := 0.01 + rand.Float64()*float64(maxValue)
		result = append(result, fmt.Sprintf("A%d,%.2f", i, value))
	}

	return result
}

func main() {

	n := flag.Int("n", 100, "number of lines to generate")
	max := flag.Int("max", 10, "maxium value to generate")

	flag.Parse()

	lines := generate(*n, *max)
	for _, line := range lines {
		fmt.Println(line)
	}
}

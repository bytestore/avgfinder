package avgfinder

import "testing"

func TestIsValidID(t *testing.T) {

	if checkID("A123") != nil {
		t.Error(`"A123" is a valid ID`)
	}

	if checkID("A12 3") == nil {
		t.Error(`"A12 3" is not a valid ID`)
	}
}

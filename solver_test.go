package avgfinder

import (
	"sort"
	"testing"
)

func TestMedian(t *testing.T) {
	a := []float64{1}
	if median(a) != 1 {
		t.Error(`Median of [1] should be 1.`)
	}

	a = []float64{1, 2, 3}
	if median(a) != 2 {
		t.Error(`Median of [1, 2, 3] should be 2.`)
	}

	a = []float64{1, 2}
	if median(a) != 1.5 {
		t.Error(`Median of [1, 2] should be 1.5.`)
	}
}

func TestFindSingleItem(t *testing.T) {
	m1 := map[string]float64{
		"A1": 1,
		"A2": 3,
		"A3": 11,
	}

	m2 := map[string]float64{
		"A1": 2.5,
		"A2": 7.5,
		"A3": 0.5,
	}

	result := FindAverageItems(m1, m2)
	if !(len(result) == 1 && result[0] == "A1") {
		t.Errorf("Result should be [A1], found: %v", result)
	}

	// median1 = 3
	// median2 = 2.5

	// A1: 2 + 0 = 2
	// A2: 0 + 5 = 5
	// A3: 8 + 2 = 10
}

func TestFindSeveralItems(t *testing.T) {
	m1 := map[string]float64{
		"A1": 1,
		"A2": 7,
		"A3": 10,
	}

	m2 := map[string]float64{
		"A1": 2.5,
		"A2": 7.5,
		"A3": 0.5,
	}

	result := FindAverageItems(m1, m2)
	sort.Strings(result)
	if !(len(result) == 2 && result[0] == "A2" && result[1] == "A3") {
		t.Errorf("The result should be [A2, A3], found: %v", result)
	}

	// median1 = 7
	// median2 = 2.5

	// A1: 6 + 0 = 6
	// A2: 0 + 5 = 5
	// A3: 3 + 2 = 5
}

# Average Item Finder

## Generate Test Data

Test data can be generated using the `generateTestData` command.

Building the command:

```
$ cd cmd/generateTestData
$ go build
```

Displaying help:

```
$ ./generateTestData -h
Usage of ./generateTestData:
  -max int
    	maxium value to generate (default 10)
  -n int
    	number of lines to generate (default 100)
```

Generate 1000 lines of test data with values between 0 and 50:

```
$ ./generateTestData -n 1000 -max 50
```

## Finding the Average Item

Building the command:

```
$ cd cmd/findAverage
$ go build
```

Running the command without any arguments shows a usage message:

```
$ ./findAverage
Usage: findAverage <weights.csv> <prices.csv>
```

Using previously generated data files (sample):
```
$ ./findAverage weights.csv prices.csv
A71
```

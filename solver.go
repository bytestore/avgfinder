package avgfinder

import (
	"fmt"
	"math"
	"os"
	"sort"
)

// FindAverageItems finds the average item with the least deviation from the median in both maps.
func FindAverageItems(m1 map[string]float64, m2 map[string]float64) []string {
	if len(m1) != len(m2) {
		fmt.Fprintln(os.Stderr, "Input sets must have the same size.")
		os.Exit(1)
	}

	var values1 []float64
	var values2 []float64
	for id, v1 := range m1 {
		v2, ok := m2[id]
		if !ok {
			fmt.Fprintf(os.Stderr, "ID only found in one set: %s\n", id)
			os.Exit(1)
		}
		values1 = append(values1, v1)
		values2 = append(values2, v2)
	}

	median1 := median(values1)
	median2 := median(values2)

	var minDeviation float64 = math.MaxFloat64
	var solutionIDs []string
	for id := range m1 {
		dev := math.Abs(m1[id]-median1) + math.Abs(m2[id]-median2)
		if dev < minDeviation {
			if len(solutionIDs) > 0 {
				solutionIDs = solutionIDs[:0]
			}
			solutionIDs = append(solutionIDs, id)
			minDeviation = dev
		} else if dev == minDeviation {
			solutionIDs = append(solutionIDs, id)
		}
	}

	return solutionIDs
}

func median(a []float64) float64 {
	sort.Float64s(a)
	if len(a)%2 == 1 {
		return a[len(a)/2]
	}
	return float64((a[len(a)/2-1] + a[len(a)/2])) / 2
}
